import { Component, OnInit } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {

   listePosts: Post[] = [{title: 'Message 1', content: 'texte 1', loveIts: 0, created_at:  new Date() },
                          {title: 'Message 2', content: 'texte 2', loveIts: 0, created_at:  new Date() },
                          {title: 'Message 3', content: 'texte 3', loveIts: 0, created_at:  new Date() }];



}
